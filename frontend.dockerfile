FROM node:14-alpine

RUN apk --no-cache add --virtual .builds-deps build-base python2 git

RUN mkdir -p /app

WORKDIR /app

RUN git clone https://gitlab.com/d8363/frontend.git

WORKDIR /app/frontend

RUN npm install

RUN npm run build

EXPOSE $PORT

ENV NUXT_HOST=0.0.0.0

ENV NUXT_PORT=$PORT

ENV PROXY_API=$PROXY_API

ENV PROXY_LOGIN=$PROXY_LOGIN

CMD [ "npm", "start" ]